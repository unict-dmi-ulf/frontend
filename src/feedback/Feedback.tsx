/* eslint-disable */
import axios from "axios";
import {
  FunctionComponent, useEffect,
  useState,
} from "react";
import { Button} from "react-bootstrap";
import { Link } from "react-router-dom";
import { getToken } from "../token/Token";

// @ts-ignore

const { REACT_APP_API_URL } = process.env;

const Feedback: FunctionComponent = () => {
  const [data, setData] = useState([]);

  let apiUrl = REACT_APP_API_URL + "/courses";
  console.log(apiUrl)

  useEffect(() => {
    getData(apiUrl);
  }, []);

  const getData = (apiUrl) => {
    let token: any = getToken()
    axios({
      method: 'get',
      url: apiUrl,
      headers: { 'content-type': 'application/json', 'Accept': '*/*', 'authorization': token }
    })
      .then(function (response) {
        console.log(response);
        return response.data
      })

      .then((json) => setData(json._embedded.courses))
      .catch((error) => {
        console.log(`Error getting ad data: ${error.message}`);
      });
  };

  console.log(data);

  return (
    <div className="Feedback">
      <h4>
        Step 3: Scegli il corso per cui stai rilasciando il Feedback
      </h4>
      {data.map((course: any) => (
        <Link
          to={{
            pathname: "course/",
            state: {
              dataUrl: course?._links?.lectures?.href,
            },
          }}
        >
          <Button
            variant="primary"
            size="lg"
          >
            {" "}
            {course.name}{" "}
          </Button>
        </Link>
      ))}
      <br />

    </div>
  );
};

export default Feedback;
