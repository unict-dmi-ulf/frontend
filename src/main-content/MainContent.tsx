 /* eslint-disable */
import {FunctionComponent} from 'react';
import { Link } from 'react-router-dom';
import './MainContent.scss';
import { Button } from 'react-bootstrap'

const MainContent: FunctionComponent = () => {

  return (
      <div className="MainContent">
        <h2>Welcome Page</h2>
        <br/>
        <h4> Step 1: Clicca su: "Inserisci Token" e scrivi il token presente nel QR Code.</h4>
        <body>
        <p style={{color: "red"}}> N.B: Devi inserire il codice correttemente altrimenti il feedback non verrà aggiunto. Ti preghiamo di fare attenzione.</p>  
        </body>
        <br/>

          <Link to="/token">
              <Button variant="primary" size="lg">
                  Inserisci Token
              </Button>
          </Link>
          <br/>
          <br/>
          <br/>
          <br/>
          <h4> Step 2: Clicca su: "Inserisci Feedback" </h4>
          <Link to="/feedback">
            <Button variant="secondary" size="lg">
              Inserisci Feedback
            </Button>
          </Link>

      </div>
    );
}


export default MainContent;
