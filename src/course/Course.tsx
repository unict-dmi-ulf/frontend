 /* eslint-disable */
import { useEffect, useState } from 'react';
import { Redirect} from 'react-router-dom';
import './Course.scss';
import { Link } from "react-router-dom";
import { Button} from "react-bootstrap";
import axios from 'axios';
import { getToken } from '../token/Token';

const Course = (props): JSX.Element => {
  const [lectures, setLectures] = useState([]);

  const dataUrl = props.location.state?.dataUrl;


  const fetchLecturesData = () => {
    let token = getToken()
    axios({
      method: 'get',
      url: dataUrl,
      headers: { 'content-type': 'application/json', 'Accept': '*/*', 'authorization': token }
    })
      .then(function (response) {
        console.log(response);
        return response.data
      })
      .then((json) => {
          setLectures(json._embedded.lectures)
      })
      .catch((error) => {
        console.log(`Error getting lectures data: ${error.message}`);
      });
    return lectures;
  };

  useEffect(() => {
    if (dataUrl==undefined){
      return
    }
    fetchLecturesData();
  }, [dataUrl]);

  if (dataUrl==undefined){
    return (<Redirect to='/home'/>);
  }

  return (
    <div className="Course">
      <h4>
        Step 4: Scegli la lezione per cui si sta rilasciando il feedback
      </h4>
     
     {lectures.map((lecture: any) => (
        <Link
          to={{
            pathname: '/course/lecture/',
            state: {
              dataUrl: lecture?._links?.questionnaire?.href,
              lectureId: lecture?.id,
            },
          }}
        >
          <Button
            variant="primary"
            size="lg"
          >
            {"Lecture: " + lecture.id}
          </Button>
        </Link>
        
      ))}
    </div>
  );
};

export default Course;
