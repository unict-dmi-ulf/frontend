import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { FunctionComponent } from 'react';
import { Nav } from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';
import './Menu.scss';




const Menu: FunctionComponent = () => {
  return (
    <div className="Menu">
      <Navbar expand="lg" className="bg-navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
        <div className="container">
          <Navbar.Brand href="#/home">
              &nbsp; ULF
          </Navbar.Brand>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto" variant="pills" activeKey="1">
              <Nav.Link href="https://gitlab.com/unict-dmi-ulf" target="_blank">
                <FontAwesomeIcon icon={faGitlab} />
              &nbsp; GitLab
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
    </div>
  );
};

export default Menu;
