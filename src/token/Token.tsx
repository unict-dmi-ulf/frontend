import React, {FunctionComponent, useState } from 'react';
import { Button } from "react-bootstrap";
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export const getToken = () => {
    try {
        const serializedState = localStorage.getItem('token');
        if (serializedState === null) {
            return undefined;
        }
        return serializedState;
    } catch (err) {
        return undefined;
    }
};

const Token: FunctionComponent = () => {

    const tmp = localStorage.getItem('token');
    const [token, setToken] = useState(tmp);

    const updateToken = (event: any): void => {
      setToken(event.target.value);
    }

    const saveToken = () => {
        try {
            if (token !== null) {
                localStorage.setItem('token', token);
            }
        } catch (err) {
            console.error(err);
            return undefined;
        }
    };

    return(
        <div className="Token">
            <Form.Group controlId="text_area_token">
                <Form.Label>Token</Form.Label>
                <Form.Control as="textarea" rows={1} onChange={(event) => updateToken(event)}>
                    {token}
                </Form.Control>
            </Form.Group>

            <Link to="/home">
                <Button onClick={() => { saveToken(); }}>Submit</Button>
            </Link>
        </div>
    );

};

export default Token;