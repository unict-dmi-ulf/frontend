 /* eslint-disable */
import React from 'react';
import {
  Switch,
  Route,
  HashRouter,
  Redirect
} from 'react-router-dom';

import Menu from './menu/Menu';
import Footer from './footer/Footer';
import './App.scss';
import MainContent from './main-content/MainContent';
import Course from './course/Course';
import Feedback from './feedback/Feedback';
import Lecture from './lecture/Lecture';
import { Link } from 'react-router-dom';
import Token from './token/Token';

function App() {
  


  return (
    <div className="App">
      <Menu />
      <br/>
      <div className="container">
          <div className="container-fluid mt-space">
              <HashRouter basename="/">
                  <Switch>
                      <Route exact path="/home" component={MainContent} />
                      <Route exact path="/feedback" component={Feedback} />
                      <Route exact path="/token" component={Token} />
                      <Route exact path="/course/" component={Course} />
                      <Route exact path="/course/lecture/" component={Lecture} />
                      <Redirect to="/home" />
                  </Switch>
              </HashRouter>
          </div>
      </div>
        <Footer />
    </div>
  );
}

export default App;
