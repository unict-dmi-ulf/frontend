/* eslint-disable */
import { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { FormControl, Form, FormCheck, Button } from "react-bootstrap";
import { getToken } from '../token/Token';
import axios from 'axios';

const Lecture = (props): JSX.Element => {

  const [questionnaires, setQuestionnaire] = useState([]);

  const answers: number[] | string[] = [];

  const setAnswer = (event, questId): void => {
    answers[questId] = event.target.value;
  }

  const dataUrl = props.location.state?.dataUrl;
  console.log(dataUrl)
  const lectureId = props.location.state?.lectureId;
  
  const fetchQuestionnaireData = () => {
    let token = getToken()
    axios({
      method: 'get',
      url: dataUrl,
      headers: { 'content-type': 'application/json', 'Accept': '*/*', 'authorization': token }
    })
      .then(function (response) {
        console.log(response);
        return response.data
      })
      .then((json) => {
        setQuestionnaire(json.questions)
      })
      .catch((error) => {
        console.log(`Error getting lectures data: ${error.message}`);
      });

    return questionnaires;
  };

  const sendQuestionnaire = () => {
    let token= getToken();

    axios({
      method: 'post',
      url: '/feedbacks',
      headers: { 'content-type': 'application/json', 'Accept': '*/*' },
      data: {
        token: token,
        lecture: lectureId,
        answers: answers
      }
    })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
    console.log("right")

  };

  const handleRadio = (questId, value) => {
    answers[questId] = value;
    console.log(answers);
  };

  const printRadio = (questId) => {
    const res: JSX.Element[] = [];
    const list = ["1", "2", "3", "4", "5"];

    for (const i of list) {
      res.push(
        <>
          <FormCheck type="radio" name="lecture" label={i} key={questId + '-' + i} onChange={(event) => handleRadio(questId, i)} ></FormCheck>
        </>
      )
    }

    return res;

  }
  useEffect(() => {
    if (dataUrl == undefined) {
      return
    }
    fetchQuestionnaireData();
  }, [dataUrl]);

  if (dataUrl == undefined) {
    //history.push("/home");
    return (<Redirect to='/home' />);
  }

  return (
    <div className="Course">
      <h4>
        Step 5: Compila il questionario e clicca su SUBMIT
      </h4>
      <Form>

        {questionnaires.map((questionnaire: any, questId: number) => (
          <div>
            <br></br>
            <Form.Label key={'label-' + questId}>
              {" "}
              {questionnaire.text}
              {" "}
            </Form.Label>

            {(() => {
              if (questionnaire.type === "OPEN_RESPONSE") {
                return (
                  // TEXT AREA
                  <FormControl as="textarea" name="answer" onChange={(event) => setAnswer(event, questId)} key={'control-' + questId} required>
                  </FormControl>
                )
              }

              return (
                <>
                  {printRadio(questId)}
                </>
              )
            }
            )()}
          </div>
        ))}

        <br></br>
        <Link to="/home">
          <Button onClick={sendQuestionnaire}>
            Submit
          </Button>
        </Link>
      </Form>
    </div>
  );
}

export default Lecture;


